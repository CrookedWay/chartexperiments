import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

#Seaborn Testing

df = pd.read_csv("test.csv", skiprows=4)
sns.lmplot('Date', 'Anomaly', data=df, fit_reg=False)